#include <stdio.h>
#include <stdlib.h>
#include "code.h"

int main(int argv, char * argc[]) {
  if (argv > 1) { 
    char* in = argc[1];
    char* out = rot13(in);
    printf("rot13(%s) = %s\n",in, out);
    return 0;
  }
  return 1;
}
