/*
  Simple substitution ciphers
*/

#ifndef CIPHER
#define CIPHER

/* Implements a simple ROT13 substitution cipher.
   See https://en.wikipedia.org/wiki/ROT13 for details.
   Examples:
   rot13("VOW") is "IBJ"
   rot13("IBJ") is "VOW"
   rot13("vow") is "ibj"
   rot13("ibj") is "vow"
 */
char* rot13(char*);

#endif
